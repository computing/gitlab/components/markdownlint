# Test

https://gitlab.com - this line would fail linting because of MD034/no-bare-urls
(but the rule is disabled in `.markdownlint-cli2.jsonc`).

Lorem Ipsum is simply dummy text of the printing and typesetting industry - this line will fail because of MD013/line-length

<!-- markdownlint-disable MD040  -->
```
This would fail because of MD040/fenced-code-language but it's disabled "inline".
Don't forget to enable rules after the exception!
```
<!-- markdownlint-enable MD040  -->
