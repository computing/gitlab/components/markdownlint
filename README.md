# Markdownlint

By default markdown lint:

- Parses all markdown (`.md`) files in the repository.\
  Override this behaviour by providing the `glob` input parameter.
- Uses default linting rules.\
  To use custom rules, follow the [markdownlint-cli2 docs](https://github.com/DavidAnson/markdownlint-cli2?tab=readme-ov-file#configuration).\
  For example, by creating a `.markdownlint-cli2.jsonc` in repository root.
- Uses output formatter `markdownlint-cli2-formatter-codequality`.\
  This provides feedback in merge requests (on the overview and changes tabs).\
  Alternate output formatters can be configured using the `formatters` input,
  or in a config file.\
  If a markdownlint config file exists in the repository,
  configure output formatters there.
